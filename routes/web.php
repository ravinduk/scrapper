<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/comparison-institutes', 'ComparisonRateInstitutesController@index')->name('comparisonInstitutes');
Route::get('/comparison-institutes/edit/{id}', 'ComparisonRateInstitutesController@edit')->name('comparisonInstitutesEdit');
Route::patch('/comparison-institutes/update/{id}','ComparisonRateInstitutesController@update')->name('comparisonInstitutesUpdate');
Route::get('/comparison-institutes/create', 'ComparisonRateInstitutesController@create')->name('comparisonInstitutesCreate');
Route::post('/comparison-institutes/store','ComparisonRateInstitutesController@store')->name('comparisonInstitutesAdd');
