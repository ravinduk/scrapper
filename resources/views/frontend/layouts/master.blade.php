<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="_token" content="{{ csrf_token() }}" />
    <title>
        @if(isset($title))
            <title>{{ $title }}</title>
        @else
            <title>Fipbox - Sri Lanka's biggest finance comparison site | Get verified rates</title>
        @endif
        @if(isset($description))
            <meta name="description" content="{{ $description }}">
        @else
            <meta name="description" content="Open a FD in 03 clicks. Get latest Fixed Deposit (FD) rates from top, Commercial Banks & Finance Companies in Sri Lanka. Compare and Invest">
        @endif
    </title>
</head>
</html>
