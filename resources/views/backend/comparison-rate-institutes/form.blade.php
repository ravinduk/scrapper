<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Type: *</strong>
            <select class="form-control" name="type">
                <option value="Bank">Bank</option>
                <option value="Finance">Finance</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Institute Name: </strong>
            <input name="institute_name" type="text" class="form-control" value="{{ old('institute_name') }}">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Image Link: </strong>
            <input name="image_link" type="text" class="form-control" value="{{ old('image_link') }}">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Rating: </strong>
            <input name="rating" type="text" class="form-control" value="{{ old('rating') }}">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Web URL: </strong>
            <input name="web_url" type="text" class="form-control" value="{{ old('web_url') }}">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="onboard" name="onboard" value="1">
            <label class="form-check-label" for="onboard" name="onboard">Onboard</label>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="status" name="status" value="1">
            <label class="form-check-label" for="onboard" name="status">Status</label>
        </div>
    </div>
</div>
