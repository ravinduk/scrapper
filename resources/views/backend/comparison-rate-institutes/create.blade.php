
@extends('layouts.app')
@section('content')
    <div class="page-content">
        <div class="fullwidth">
            <div class="container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 joint-account">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::open(array('route' => 'comparisonInstitutesAdd','method'=>'POST', 'enctype'=>'multipart/form-data')) !!}
                            @include('backend.comparison-rate-institutes.form')
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="margin-bottom: 10px;">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-right">
                                <a class="btn btn-dark" href="{{ route('comparisonInstitutes') }}"> Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
