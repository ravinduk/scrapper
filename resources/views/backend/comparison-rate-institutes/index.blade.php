@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="input-group">
                <a href="{{ route('comparisonInstitutesCreate') }}"><button class="btn btn-primary" type="submit"><i class="fa fa-search fa-fw"></i> Add New Member</button></a>
            </div>
            <br>
        </div>
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="tablewrapper" id="irtcptable">
                <table class="table table-hover table-dark table-bordered">

                    <tr>
                        <th>Institute name</th>
                        <th>Web URL</th>
                        <th>Edit</th>
                    </tr>
                    @foreach($data as $datas)
                    <tr>
                        <td>{{ $datas->institute_name }}</td>
                        <td>{{ $datas->web_url }}</td>
                        <td><a href="{{ url('/') }}/comparison-institutes/edit/{{$datas->id}}">Edit</a></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
