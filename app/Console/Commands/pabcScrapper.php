<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class pabcScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:pabc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PABC FD rates scrapper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'https://www.pabcbank.com/rates/#716-v');

        $thead = $crawler->filter('table')->eq(7)->filter('th')->each(function ($tr, $i) {
            return $tr->filter('th')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        print_r($thead);
        $data = $crawler->filter('table')->eq(7)->filter('td')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        print_r($data);

        if($thead[1][0] == "1 Month"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[1][0]);
            $maturity_rate = str_replace( '%', '', $data[16][0]);
            $newbank->maturity_rate = $maturity_rate;
            $newbank->save();
        }
        if($thead[2][0] == "3 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[2][0]);
            $maturity_rate = str_replace( '%', '', $data[17][0]);
            $newbank->maturity_rate = $maturity_rate;
            $newbank->save();
        }
        if($thead[3][0] == "4 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[3][0]);
            $maturity_rate = str_replace( '%', '', $data[18][0]);
            $newbank->maturity_rate = $maturity_rate;
            $newbank->save();
        }
        if($thead[4][0] == "6 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[4][0]);
            $maturity_rate = str_replace( '%', '', $data[20][0]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[19][0]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();
        }
        if($thead[5][0] == "12 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[5][0]);
            $maturity_rate = str_replace( '%', '', $data[22][0]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[21][0]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();
        }
        if($thead[6][0] == "24 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[6][0]);
            $maturity_rate = str_replace( '%', '', $data[24][0]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[23][0]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();
        }
        if($thead[7][0] == "36 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[7][0]);
            $maturity_rate = str_replace( '%', '', $data[26][0]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[25][0]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();
        }
        if($thead[8][0] == "48 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[8][0]);
            $maturity_rate = str_replace( '%', '', $data[28][0]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[27][0]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();
        }
        if($thead[9][0] == "60 Months"){
            $newbank = new ComparisonRate();
            $newbank->institute = '2';
            $newbank->rating = "AA";
            $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[9][0]);
            $maturity_rate = str_replace( '%', '', $data[30][0]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[29][0]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();
        }
    }
}
