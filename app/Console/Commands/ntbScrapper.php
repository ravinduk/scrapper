<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class ntbScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:ntb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'NTB FD Rates Scrapper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'https://www.nationstrust.com/deposit-rates');

        $thead = $crawler->filter('.table-bordered')->eq(0)->filter('th')->each(function ($tr, $i) {
            return $tr->filter('th')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        print_r($thead);
        $tdata = $crawler->filter('.table-bordered')->eq(0)->filter('td')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        print_r($tdata);

            if($thead[2][0] == "1 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[2][0]);
                $maturity_rate = $this->getStructuredRate($tdata[2][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[2][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
            if($thead[3][0] == "3 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[3][0]);
                $maturity_rate = $this->getStructuredRate($tdata[3][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[3][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
            if($thead[4][0] == "6 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[4][0]);
                $maturity_rate = $this->getStructuredRate($tdata[4][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[4][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
            if($thead[5][0] == "12 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[5][0]);
                $maturity_rate = $this->getStructuredRate($tdata[5][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[5][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
            if($thead[6][0] == "24 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[6][0]);
                $maturity_rate = $this->getStructuredRate($tdata[6][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[6][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
            if($thead[7][0] == "36 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[7][0]);
                $maturity_rate = $this->getStructuredRate($tdata[7][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[7][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
            if($thead[8][0] == "48 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[8][0]);
                $maturity_rate = $this->getStructuredRate($tdata[8][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[8][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
            if($thead[9][0] == "60 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = 2;
                $newbank->rating = "AA";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[9][0]);
                $maturity_rate = $this->getStructuredRate($tdata[9][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[9][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();
            }
    }
    public function getStructuredRate($rate){
        return ($rate == "-")?null : floatval(preg_replace("/[^0-9.]/", "", $rate));
    }
}
