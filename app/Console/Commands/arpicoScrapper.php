<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class arpicoScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:arpico';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Arpico finance fd rate scrapper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'http://www.arpicofinance.com/fixed-deposits/');

        $data = $crawler->filter('table')->eq(0)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        print_r($data);

        foreach ($data as $key => $datas){
            if($key > 0 && $datas[0] != "-") {
                $newbank = new ComparisonRate();
                $newbank->institute = '21';
                $newbank->rating = "A+";
                $month = preg_replace('/[^0-9]/', '', $datas[0]);
                $newbank->number_of_months = $month;
                $newbank->maturity_rate = $this->getStructuredRate($datas[3]);
                $newbank->monthly_rate = $this->getStructuredRate($datas[1]);
                $newbank->save();
            }
        }
    }
    private function getStructuredRate($rate){
        return ($rate == "-")?null : floatval(preg_replace("/[^0-9.]/", "", $rate));
    }
}
