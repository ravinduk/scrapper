<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class bocScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:com';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapping Commercial Bank rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'https://www.combank.lk/newweb/en/personal/deposits/fixed-deposits');

        $data = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });

        foreach ($data as $key => $tests) {
            if ($key > 0 && $key < 5) {
                $newbank = new ComparisonRate();
                $newbank->institute = '3';
                $newbank->rating = "AA-";
                $month = preg_replace('/[^0-9]/', '', $tests[0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace('%', '', $tests[2]);
                $newbank->maturity_rate = $maturity_rate;
                $newbank->save();
            }
        }
            //12 months
                $newbank = new ComparisonRate();
                $newbank->institute = '1';
                $newbank->rating = "AAA";
                $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[5][0]);
                $maturity_rate = str_replace( '%', '', $data[6][2]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = str_replace( '%', '', $data[5][2]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->save();

            //24 months
            $newbank = new ComparisonRate();
            $newbank->institute = '1';
            $newbank->rating = "AAA";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[7][0]);
            $maturity_rate = str_replace( '%', '', $data[9][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[7][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();

            //36 months
            $newbank = new ComparisonRate();
            $newbank->institute = '1';
            $newbank->rating = "AAA";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[10][0]);
            $maturity_rate = str_replace( '%', '', $data[12][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[10][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();

            //48 months
            $newbank = new ComparisonRate();
            $newbank->institute = '1';
            $newbank->rating = "AAA";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[13][0]);
            $maturity_rate = str_replace( '%', '', $data[15][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[13][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();

            //60 months
            $newbank = new ComparisonRate();
            $newbank->institute = '1';
            $newbank->rating = "AAA";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[16][0]);
            $maturity_rate = str_replace( '%', '', $data[18][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[16][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->save();
    }
}
