<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class peoplesmerchantScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:pm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Peoples Merchant FD rates Scrapper.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'http://peoplesmerchant.lk/fixed-deposit/');

        $data = $crawler->filter('table')->eq(0)->filter('tr')->each(function ($tr) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });

        $instituteId = 23;
        foreach ($data as $key => $datas){
            if($key > 0 && $key < 8) {
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "A+";

                if($key == 3){
                    $newbank->number_of_months = 12;
                }
                elseif($key == 4){
                    $newbank->number_of_months = 24;
                }
                elseif($key == 5){
                    $newbank->number_of_months = 36;
                }
                elseif($key == 6){
                    $newbank->number_of_months = 48;
                }
                elseif($key == 7){
                    $newbank->number_of_months = 60;
                }
                else{
                    $month = preg_replace('/[^0-9]/', '', $datas[0]);
                    $newbank->number_of_months = $month;
                }
                $newbank->maturity_rate = $this->getStructuredRate($datas[3]);
                $newbank->monthly_rate = $this->getStructuredRate($datas[1]);
                $newbank->save();
            }
        }
    }
    private function getStructuredRate($rate){
        return ($rate == "-")?null : floatval(preg_replace("/[^0-9.]/", "", $rate));
    }
}
