<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class lolcScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:lolc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LOLC FD rates Scrapper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'https://www.lolcfinance.com/savings-and-deposits/general-fixed-deposit-accounts/');

        $data = $crawler->filter('#tablepress-1')->eq(0)->filter('tr')->each(function ($tr) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        print_r($data);

        foreach ($data as $key => $datas){
            if($key > 1 && $datas[0] != "-") {
                $newbank = new ComparisonRate();
                $newbank->institute = '9';
                $newbank->rating = "A-";
                $month = preg_replace('/[^0-9]/', '', $datas[0]);
                $newbank->number_of_months = $month;
                $maturity_rate = explode( '%', $datas[5]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate[0]);
                $monthly_rate = explode( '%', $datas[1]);
                $newbank->monthly_rate = $this->getStructuredRate($monthly_rate[0]);
                $newbank->save();
            }
        }
    }
    private function getStructuredRate($rate){
        return ($rate == "-")?null : floatval(preg_replace("/[^0-9.]/", "", $rate));
    }
}
