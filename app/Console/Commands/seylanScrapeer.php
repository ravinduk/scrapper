<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class seylanScrapeer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:seylan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapping seylan FD rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'https://www.seylan.lk/interest-rates');

        $data_maturity = $crawler->filter('table')->eq(8)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
//        print_r($data_maturity);
        $data_monthly = $crawler->filter('table')->eq(9)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
//        print_r($data_monthly);
        foreach ($data_maturity as $key => $datamaturity) {
            if ($key > 0 && $key < 4) {
                $newbank = new ComparisonRate();
                $newbank->institute = '5';
                $newbank->rating = "AA-";
                $month = preg_replace('/[^0-9]/', '', $datamaturity[0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace('%', '', $datamaturity[1]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                $newbank->save();
            }
        }
        if($data_maturity[4][0] == "12 MONTHS"){
            $newbank = new ComparisonRate();
            $newbank->institute = '5';
            $newbank->rating = "AA-";
            $month = preg_replace('/[^0-9]/', '', $data_maturity[4][0]);
            $newbank->number_of_months = $month;
            $maturity_rate = str_replace( '%', '', $data_maturity[4][1]);
            $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
            $monthly_rate = str_replace( '%', '', $data_monthly[1][1]);
            $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
            $newbank->save();
        }
        if($data_maturity[5][0] == "24 MONTHS"){
            $newbank = new ComparisonRate();
            $newbank->institute = '5';
            $newbank->rating = "AA-";
            $month = preg_replace('/[^0-9]/', '', $data_maturity[5][0]);
            $newbank->number_of_months = $month;
            $maturity_rate = str_replace( '%', '', $data_maturity[5][1]);
            $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
            $monthly_rate = str_replace( '%', '', $data_monthly[2][1]);
            $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
            $newbank->save();
        }
        if($data_maturity[6][0] == "36 MONTHS"){
            $newbank = new ComparisonRate();
            $newbank->institute = '5';
            $newbank->rating = "AA-";
            $month = preg_replace('/[^0-9]/', '', $data_maturity[6][0]);
            $newbank->number_of_months = $month;
            $maturity_rate = str_replace( '%', '', $data_maturity[6][1]);
            $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
            $monthly_rate = str_replace( '%', '', $data_monthly[3][1]);
            $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
            $newbank->save();
        }
        if($data_maturity[7][0] == "48 MONTHS"){
            $newbank = new ComparisonRate();
            $newbank->institute = '5';
            $newbank->rating = "AA-";
            $month = preg_replace('/[^0-9]/', '', $data_maturity[7][0]);
            $newbank->number_of_months = $month;
            $maturity_rate = str_replace( '%', '', $data_maturity[7][1]);
            $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
            $monthly_rate = str_replace( '%', '', $data_monthly[4][1]);
            $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
            $newbank->save();
        }
        if($data_maturity[8][0] == "60 MONTHS"){
            $newbank = new ComparisonRate();
            $newbank->institute = '5';
            $newbank->rating = "AA-";
            $month = preg_replace('/[^0-9]/', '', $data_maturity[8][0]);
            $newbank->number_of_months = $month;
            $maturity_rate = str_replace( '%', '', $data_maturity[8][1]);
            $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
            $monthly_rate = str_replace( '%', '', $data_monthly[5][1]);
            $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
            $newbank->save();
        }

    }
    private function getStructuredRate($rate){
        return ($rate == "-")?null : floatval(preg_replace("/[^0-9.]/", "", $rate));
    }
}
