<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Console\Command;

class nsbScrapper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:hnb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'NSB Scrapping FD rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 600,
        ));
        $goutteClient->setClient($guzzleClient);

        $crawler = $goutteClient->request('GET', 'https://www.hnb.net/fixed-deposits-interest-rates');

        $data = $crawler->filter('#rate')->filter('.tr-element-binder')->each(function ($tr, $i) {
            return $tr->filter('.result-container')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
//        print_r($data);
        foreach ($data as $key => $dataset){
                $newbank = new ComparisonRate();
                $newbank->institute = '5';
                $newbank->rating = "AA-";
                $month = preg_replace('/[^0-9]/', '', $dataset[0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace( '%', '', $dataset[5]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                $monthly_rate = str_replace( '%', '', $dataset[1]);
                $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
                $newbank->save();
        }
    }
    private function getStructuredRate($rate){
        return ($rate == "-")?null : floatval(preg_replace("/[^0-9.]/", "", $rate));
    }

}
