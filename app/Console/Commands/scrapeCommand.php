<?php

namespace App\Console\Commands;

use App\Models\ComparisonRate;
use App\Models\ComparisonRateInstitutes;
use App\Services\ArpicoFinance;
use App\Services\CommercialLeasing;
use App\Services\SeylanBank;
use App\Services\VallibelFinance;
use Carbon\Carbon;
use Carbon\Traits\Date;
use Illuminate\Console\Command;
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;
use GuzzleHttp\Client as GuzzleClient;
use App\Services\NationalDevelopmentBank;
use App\Services\HattonNationalBank;
use App\Services\SampathBank;
use App\Services\NationsTrustBank;
use App\Services\CommercialBank;
use App\Services\CommercialCredit;
use App\Services\IdealFinance;
use App\Services\PeopleMerchantFinance;
use App\Services\PeoplesLeasing;
use App\Services\RegionalDevelopmentBank;

class scrapeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapper:start';

    protected $allInstitute;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->allInstitute = ComparisonRateInstitutes::all();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $goutteClient->setClient($guzzleClient);

        foreach ($this->allInstitute as $institutes)
        {
            $institute_name = $institutes->institute_name;
            $institute_url = $institutes->web_url;

            if($institute_name == "National Development Bank"){
                $ndb = new NationalDevelopmentBank();
                $crawler = $goutteClient->request('GET', $institute_url);
                $ndb->nationalDevelopmentBank($crawler);
            }
            if($institute_name == "Hatton National Bank"){
                $hnb = new HattonNationalBank();
                $crawler = $goutteClient->request('GET', $institute_url);
                $hnb->hattonNationalBank($crawler);
            }
            if($institute_name == "Sampath Bank PLC"){
                $sampath = new SampathBank();
                $crawler = $goutteClient->request('GET', $institute_url);
                $sampath->sampathBank($crawler);
            }
            if($institute_name == "Nations Trust Bank PLC"){
                $ntb = new NationsTrustBank();
                $crawler = $goutteClient->request('GET', $institute_url);
                $ntb->nationsTrustBank($crawler);
            }
            if($institute_name == "Commercial Bank of Ceylon"){
                $com = new CommercialBank();
                $crawler = $goutteClient->request('GET', $institute_url);
                $com->commercialBank($crawler);
            }
            if($institute_name == "Commercial Credit & Finance Plc"){
                $clc = new CommercialCredit();
                $crawler = $goutteClient->request('GET', $institute_url);
                $clc->commercialCredit($crawler);
            }
            if($institute_name == "Commercial Leasing & Finance PLC"){
                $clc = new CommercialLeasing();
                $crawler = $goutteClient->request('GET', $institute_url);
                $clc->commercialLeasing($crawler);
            }
            if($institute_name == "Ideal Finance Ltd"){
                $ideal = new IdealFinance();
                $crawler = $goutteClient->request('GET', $institute_url);
                $ideal->idealsFinance($crawler);
            }
            if($institute_name == "People Merchant Finance PLC"){
                $pmc = new PeopleMerchantFinance();
                $crawler = $goutteClient->request('GET', $institute_url);
                $pmc->peopleMerchantFinance($crawler);
            }
            if($institute_name == "Peoples Leasing & Finance PLC"){
                $plc = new PeoplesLeasing();
                $crawler = $goutteClient->request('GET', $institute_url);
                $plc->peoplesLeasing($crawler);
            }
            if($institute_name == "Regional Development Bank"){
                $rdb = new RegionalDevelopmentBank();
                $crawler = $goutteClient->request('GET', $institute_url);
                $rdb->regionalDevelopmentBank($crawler);
            }
            if($institute_name == "Seylan Bank"){
                $seylan = new SeylanBank();
                $crawler = $goutteClient->request('GET', $institute_url);
                $seylan->seylanBank($crawler);
            }
            if($institute_name == "Arpico Finance"){
                $arpico = new ArpicoFinance();
                $crawler = $goutteClient->request('GET', $institute_url);
                $arpico->arpicoFinance($crawler);
            }
            if($institute_name == "Vallibel Finance"){
                $vallibel = new VallibelFinance();
                $crawler = $goutteClient->request('GET', $institute_url);
                $vallibel->vallibelFinance($crawler);
            }

        }
    }
}
