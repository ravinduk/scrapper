<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComparisonRate extends Model
{
    protected $table = 'comparison_rates';

    protected $fillable = ['institute','rating','number_of_months','maturity_rate','monthly_rate','validity_date'];
}
