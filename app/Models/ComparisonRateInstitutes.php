<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComparisonRateInstitutes extends Model
{
    protected $table = 'comparison_rate_institutes';

    protected $fillable = ['type','institute_name','image_link','onboarded','status','issuer_id','web_url'];
}
