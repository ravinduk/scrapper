<?php


namespace App\Traits;


trait PublicTraits
{
    public function getStructuredRate($rate){
        return ($rate == "-")?null : floatval(preg_replace("/[^0-9.]/", "", $rate));
    }
}
