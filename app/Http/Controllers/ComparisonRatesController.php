<?php

namespace App\Http\Controllers;

use App\Models\ComparisonRate;
use App\Models\ComparisonRateInstitutes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComparisonRatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lastRecord = DB::table('comparison_rates')->select('validity_date')->orderBy('id', 'desc')->limit(1)->get()->first();
        $rates = ComparisonRate::where('validity_date',$lastRecord->validity_date)->get();
        $banks = ComparisonRateInstitutes::where('type','Bank')->get();
        $finance_company = ComparisonRateInstitutes::where('type','Finance')->get();

        $data = [
            "bank" => [],
            "finance" => [],
        ];

        $monthArray = ["01 Month","02 Months","03 Months","06 Months","09 Months","12 Months"];

        foreach ($banks as $bank){
            foreach($rates as $rate){
                if($bank->id == $rate->institute){
                    if ($rate->number_of_months == 1){
                        $data["bank"][$bank['institute_name']]["01 Month"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 2){
                        $data["bank"][$bank['institute_name']]["02 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 3){
                        $data["bank"][$bank['institute_name']]["03 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 6){
                        $data["bank"][$bank['institute_name']]["06 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 9){
                        $data["bank"][$bank['institute_name']]["09 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 12){
                        $data["bank"][$bank['institute_name']]["12 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 24){
                        $data["bank"][$bank['institute_name']]["24 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 36){
                        $data["bank"][$bank['institute_name']]["36 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 48){
                        $data["bank"][$bank['institute_name']]["48 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 60){
                        $data["bank"][$bank['institute_name']]["60 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $bank->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                }
            }

        }
        foreach ($finance_company as $financeCompanies) {
            foreach ($rates as $rate) {
                if ($financeCompanies->id == $rate->institute) {
                    if ($rate->number_of_months == 1){
                        $data["finance"][$financeCompanies['institute_name']]["01 Month"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 2){
                        $data["finance"][$financeCompanies['institute_name']]["02 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 3){
                        $data["finance"][$financeCompanies['institute_name']]["03 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 6){
                        $data["finance"][$financeCompanies['institute_name']]["06 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 9){
                        $data["finance"][$financeCompanies['institute_name']]["09 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 12){
                        $data["finance"][$financeCompanies['institute_name']]["12 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 24){
                        $data["finance"][$financeCompanies['institute_name']]["24 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 36){
                        $data["finance"][$financeCompanies['institute_name']]["36 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 48){
                        $data["finance"][$financeCompanies['institute_name']]["48 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                    if ($rate->number_of_months == 60){
                        $data["finance"][$financeCompanies['institute_name']]["60 Months"][] = [
                            "product_type" => "Fixed Deposit",
                            "institute_rating" => $financeCompanies->rating,
                            "fd_date" => $rate->validity_date,
                            "rate_monthly" => $rate->monthly_rate,
                            "rate_maturity" => $rate->maturity_rate,
                        ] ;
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
