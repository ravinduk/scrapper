<?php

namespace App\Http\Controllers;

use App\Models\ComparisonRateInstitutes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComparisonRateInstitutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ComparisonRateInstitutes::get()->all();
        return view('backend.comparison-rate-institutes.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.comparison-rate-institutes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'type' => 'required',
            'institute_name' => 'required',
            'web_url' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $add_institute = new ComparisonRateInstitutes();
            $add_institute->type = $request->input('type');
            $add_institute->institute_name = $request->input('institute_name');
            $add_institute->image_link = $request->input('image_link');
            $add_institute->onboarded = $request->input('onboard');
            $add_institute->status = $request->input('status');
            $add_institute->rating = $request->input('rating');
            $add_institute->web_url = $request->input('web_url');

            $add_institute->save();
            DB::commit();

            return redirect()->route('comparisonInstitutes')
                ->with('success','Institute Added added successfully');
        }
        catch (\Exception $e){
            DB::rollback();

            return redirect()->route('comparisonInstitutes')
                ->with('error','Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institute = ComparisonRateInstitutes::find($id);
        return view('backend.comparison-rate-institutes.edit', compact('institute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'web_url' => 'required',
        ]);
        try {
            DB::beginTransaction();

            $institute = ComparisonRateInstitutes::find($id);
            $institute->type = $request->input('type');
            $institute->institute_name = $request->input('institute_name');
            $institute->image_link = $request->input('image_link');
            $institute->rating = $request->input('rating');
            $institute->web_url = $request->input('web_url');
            $institute->save();

            DB::commit();

            return redirect()->route('comparisonInstitutes')
                ->with('success', 'Web URL updated Updated successfully');
        }
        catch (\Exception $e) {
            DB::rollback();

            return redirect()->route('comparisonInstitutes')
                ->with('error', 'Something went wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
