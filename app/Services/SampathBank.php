<?php


namespace App\Services;
use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class SampathBank
{
    use PublicTraits;
    public function sampathBank($crawler){
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 6;
        $data = $crawler->filter('table')->eq(1)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        try {
            foreach ($data as $key => $datas){
                if($key > 0 && $datas[0] != "-") {
                    $newbank = new ComparisonRate();
                    $newbank->institute = $instituteId;
                    $newbank->rating = "-";
                    $month = preg_replace('/[^0-9]/', '', $datas[0]);
                    $newbank->number_of_months = $month;
                    $maturity_rate = explode( '%', $datas[1]);
                    $newbank->maturity_rate = $this->getStructuredRate($maturity_rate[0]);
                    $monthly_rate = explode( '%', $datas[2]);
                    $newbank->monthly_rate = $this->getStructuredRate($monthly_rate[0]);
                    $newbank->validity_date = $validity_date;
                    $newbank->save();
                }
            }
        }

        catch(\Exception $exception){
            print_r('Something went wrong in Sampath Bank'."\n");
        }
    }
}
