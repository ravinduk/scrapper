<?php


namespace App\Services;


use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class HattonNationalBank
{
    use PublicTraits;
    public function hattonNationalBank($crawler){
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 1;
        $data = $crawler->filter('#rate')->filter('.tr-element-binder')->each(function ($tr, $i) {
            return $tr->filter('.result-container')->each(function ($td, $i) {
                return trim($td->text());
            });
        });

    try{
        foreach ($data as $key => $dataset){
            $newbank = new ComparisonRate();
            $newbank->institute = $instituteId;
            $newbank->rating = "-";
            $month = preg_replace('/[^0-9]/', '', $dataset[0]);
            $newbank->number_of_months = $month;
            $maturity_rate = str_replace( '%', '', $dataset[5]);
            $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
            $monthly_rate = str_replace( '%', '', $dataset[1]);
            $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
            $newbank->validity_date = $validity_date;
            $newbank->save();
        }
    }
    catch(\Exception $exception){
            print_r('Something went wrong in Hatton National Bank'."\n");
        }
    }
}
