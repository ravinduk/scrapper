<?php


namespace App\Services;


use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class CommercialCredit
{
    use PublicTraits;

    public function commercialCredit($crawler)
    {
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 27;
        $data = $crawler->filter('#tblFD')->eq(0)->filter('tr')->each(function ($tr) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        try {
            foreach ($data as $key => $datas){
                if($key > 1 && $datas[0] != "-") {
                    $newbank = new ComparisonRate();
                    $newbank->institute = $instituteId;
                    $newbank->rating = "-";
                    $month = preg_replace('/[^0-9]/', '', $datas[0]);
                    $newbank->number_of_months = $month;
                    $newbank->maturity_rate = $this->getStructuredRate($datas[5]);
                    $newbank->monthly_rate = $this->getStructuredRate($datas[1]);
                    $newbank->validity_date = $validity_date;
                    $newbank->save();
                }
            }
        }
        catch(\Exception $exception){
            print_r('Something went wrong in Commercial Credit'."\n");
        }
    }
}
