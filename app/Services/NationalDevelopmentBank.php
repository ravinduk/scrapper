<?php


namespace App\Services;

use App\Models\ComparisonRate;
use App\Services\Traits;
use Carbon\Carbon;

class NationalDevelopmentBank
{
    public function nationalDevelopmentBank($crawler){
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 12;
        $test = $crawler->filter('.u-outline-tbl')->eq(2)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        try {
            foreach ($test as $key => $tests){
                if($key > 0 && $key < 3){
                    $newbank = new ComparisonRate();
                    $newbank->institute = $instituteId;
                    $newbank->rating = "-";
                    $month = preg_replace('/[^0-9]/', '', $tests[0]);
                    $newbank->number_of_months = $month;
                    $maturity_rate = str_replace( '%', '', $tests[1]);
                    $newbank->maturity_rate = $maturity_rate;
                    $newbank->validity_date = $validity_date;
                    $newbank->save();
                }
            }
            if($test[3][0] == "1 Year"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months = 12;
                $maturity_rate = str_replace( '%', '', $test[5][1]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = str_replace( '%', '', $test[4][1]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($test[6][0] == "2 Years"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months = 24;
                $maturity_rate = str_replace( '%', '', $test[8][1]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = str_replace( '%', '', $test[7][1]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($test[9][0] == "3 Years"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months = 36;
                $maturity_rate = str_replace( '%', '', $test[11][1]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = str_replace( '%', '', $test[10][1]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($test[12][0] == "4 Years"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months = 48;
                $maturity_rate = str_replace( '%', '', $test[14][1]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = str_replace( '%', '', $test[13][1]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($test[15][0] == "5 Years"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months = 60;
                $maturity_rate = str_replace( '%', '', $test[17][1]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = str_replace( '%', '', $test[16][1]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
        }
        catch(\Exception $exception){
            print_r('Something went wrong in National Development Bank'."\n");
        }
    }
}
