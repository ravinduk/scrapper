<?php


namespace App\Services;


use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class SeylanBank
{
    use PublicTraits;
    public function seylanBank($crawler){
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 25;
        $data_maturity = $crawler->filter('table')->eq(8)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        $data_monthly = $crawler->filter('table')->eq(9)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        try {
            foreach ($data_maturity as $key => $datamaturity) {
                if ($key > 0 && $key < 4) {
                    $newbank = new ComparisonRate();
                    $newbank->institute = $instituteId;
                    $newbank->rating = "-";
                    $month = preg_replace('/[^0-9]/', '', $datamaturity[0]);
                    $newbank->number_of_months = $month;
                    $maturity_rate = str_replace('%', '', $datamaturity[1]);
                    $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                    $newbank->validity_date = $validity_date;
                    $newbank->save();
                }
            }
            if($data_maturity[4][0] == "12 MONTHS"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $month = preg_replace('/[^0-9]/', '', $data_maturity[4][0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace( '%', '', $data_maturity[4][1]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                $monthly_rate = str_replace( '%', '', $data_monthly[1][1]);
                $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($data_maturity[5][0] == "24 MONTHS"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $month = preg_replace('/[^0-9]/', '', $data_maturity[5][0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace( '%', '', $data_maturity[5][1]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                $monthly_rate = str_replace( '%', '', $data_monthly[2][1]);
                $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($data_maturity[6][0] == "36 MONTHS"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $month = preg_replace('/[^0-9]/', '', $data_maturity[6][0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace( '%', '', $data_maturity[6][1]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                $monthly_rate = str_replace( '%', '', $data_monthly[3][1]);
                $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($data_maturity[7][0] == "48 MONTHS"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $month = preg_replace('/[^0-9]/', '', $data_maturity[7][0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace( '%', '', $data_maturity[7][1]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                $monthly_rate = str_replace( '%', '', $data_monthly[4][1]);
                $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($data_maturity[8][0] == "60 MONTHS"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $month = preg_replace('/[^0-9]/', '', $data_maturity[8][0]);
                $newbank->number_of_months = $month;
                $maturity_rate = str_replace( '%', '', $data_maturity[8][1]);
                $newbank->maturity_rate = $this->getStructuredRate($maturity_rate);
                $monthly_rate = str_replace( '%', '', $data_monthly[5][1]);
                $newbank->monthly_rate = $this->getStructuredRate($monthly_rate);
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
        }

        catch(\Exception $exception){
            print_r('Something went wrong in Seylan Bank'."\n");
        }
    }
}
