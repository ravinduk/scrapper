<?php


namespace App\Services;


use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class ArpicoFinance
{
    use PublicTraits;
    public function arpicoFinance($crawler)
    {
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 26;
        $data = $crawler->filter('table')->eq(0)->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });

        try {
            foreach ($data as $key => $datas){
                if($key > 0 && $datas[0] != "-") {
                    $newbank = new ComparisonRate();
                    $newbank->institute = $instituteId;
                    $newbank->rating = "-";
                    $month = preg_replace('/[^0-9]/', '', $datas[0]);
                    $newbank->number_of_months = $month;
                    $newbank->maturity_rate = $this->getStructuredRate($datas[3]);
                    $newbank->monthly_rate = $this->getStructuredRate($datas[1]);
                    $newbank->validity_date = $validity_date;
                    $newbank->save();
                }
            }
        }
        catch(\Exception $exception){
            print_r('Something went wrong in Arpico Finance'."\n");
        }
    }
}
