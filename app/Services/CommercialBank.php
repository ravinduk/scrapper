<?php


namespace App\Services;


use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class CommercialBank
{
 use PublicTraits;
    public function commercialBank($crawler)
    {
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 4;
        $data = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        try {
            foreach ($data as $key => $tests) {
                if ($key > 0 && $key < 5) {
                    $newbank = new ComparisonRate();
                    $newbank->institute = $instituteId;
                    $newbank->rating = "-";
                    $month = preg_replace('/[^0-9]/', '', $tests[0]);
                    $newbank->number_of_months = $month;
                    $maturity_rate = str_replace('%', '', $tests[2]);
                    $newbank->maturity_rate = $maturity_rate;
                    $newbank->validity_date = $validity_date;
                    $newbank->save();
                }
            }
            //12 months
            $newbank = new ComparisonRate();
            $newbank->institute = $instituteId;
            $newbank->rating = "-";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[5][0]);
            $maturity_rate = str_replace( '%', '', $data[6][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[5][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->validity_date = $validity_date;
            $newbank->save();

            //24 months
            $newbank = new ComparisonRate();
            $newbank->institute = $instituteId;
            $newbank->rating = "-";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[7][0]);
            $maturity_rate = str_replace( '%', '', $data[9][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[7][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->validity_date = $validity_date;
            $newbank->save();

            //36 months
            $newbank = new ComparisonRate();
            $newbank->institute = $instituteId;
            $newbank->rating = "-";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[10][0]);
            $maturity_rate = str_replace( '%', '', $data[12][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[10][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->validity_date = $validity_date;
            $newbank->save();

            //48 months
            $newbank = new ComparisonRate();
            $newbank->institute = $instituteId;
            $newbank->rating = "-";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[13][0]);
            $maturity_rate = str_replace( '%', '', $data[15][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[13][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->validity_date = $validity_date;
            $newbank->save();

            //60 months
            $newbank = new ComparisonRate();
            $newbank->institute = $instituteId;
            $newbank->rating = "-";
            $newbank->number_of_months = preg_replace('/[^0-9]/', '', $data[16][0]);
            $maturity_rate = str_replace( '%', '', $data[18][2]);
            $newbank->maturity_rate = $maturity_rate;
            $monthly_rate = str_replace( '%', '', $data[16][2]);
            $newbank->monthly_rate = $monthly_rate;
            $newbank->validity_date = $validity_date;
            $newbank->save();
        }
        catch(\Exception $exception){
            print_r('Something went wrong in Commercial Bank'."\n");
        }

    }
}
