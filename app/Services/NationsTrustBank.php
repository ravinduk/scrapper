<?php


namespace App\Services;


use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class NationsTrustBank
{
    use PublicTraits;
    public function nationsTrustBank($crawler){
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 10;
        $thead = $crawler->filter('.table-bordered')->eq(0)->filter('th')->each(function ($tr, $i) {
            return $tr->filter('th')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        $tdata = $crawler->filter('.table-bordered')->eq(0)->filter('td')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });

        try {
            if($thead[2][0] == "1 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[2][0]);
                $maturity_rate = $this->getStructuredRate($tdata[2][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[2][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($thead[3][0] == "3 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[3][0]);
                $maturity_rate = $this->getStructuredRate($tdata[3][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[3][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($thead[4][0] == "6 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[4][0]);
                $maturity_rate = $this->getStructuredRate($tdata[4][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[4][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($thead[5][0] == "12 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[5][0]);
                $maturity_rate = $this->getStructuredRate($tdata[5][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[5][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($thead[6][0] == "24 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[6][0]);
                $maturity_rate = $this->getStructuredRate($tdata[6][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[6][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($thead[7][0] == "36 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[7][0]);
                $maturity_rate = $this->getStructuredRate($tdata[7][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[7][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($thead[8][0] == "48 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[8][0]);
                $maturity_rate = $this->getStructuredRate($tdata[8][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[8][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
            if($thead[9][0] == "60 month"){
                $newbank = new ComparisonRate();
                $newbank->institute = $instituteId;
                $newbank->rating = "-";
                $newbank->number_of_months =  preg_replace('/[^0-9]/', '', $thead[9][0]);
                $maturity_rate = $this->getStructuredRate($tdata[9][0]);
                $newbank->maturity_rate = $maturity_rate;
                $monthly_rate = $this->getStructuredRate($tdata[9][0]);
                $newbank->monthly_rate = $monthly_rate;
                $newbank->validity_date = $validity_date;
                $newbank->save();
            }
        }
        catch(\Exception $exception){
            print_r('Something went wrong in Nation Trust Bank'."\n");
        }
    }
}
