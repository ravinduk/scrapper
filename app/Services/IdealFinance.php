<?php


namespace App\Services;


use App\Models\ComparisonRate;
use App\Traits\PublicTraits;
use Carbon\Carbon;

class IdealFinance
{
    use PublicTraits;
    public function idealsFinance($crawler)
    {
        $validity_date = Carbon::now()->toDateString();
        $instituteId = 19;
        $data = $crawler->filter('table')->eq(0)->filter('tr')->each(function ($tr) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });
        try{
            foreach ($data as $key => $datas){
                if($key > 1) {
                    $newbank = new ComparisonRate();
                    $newbank->institute = $instituteId;
                    $newbank->rating = "-";

                    if($key == 5){
                        $newbank->number_of_months = 12;
                    }
                    elseif($key == 6){
                        $newbank->number_of_months = 24;
                    }
                    elseif($key == 7){
                        $newbank->number_of_months = 36;
                    }
                    elseif($key == 8){
                        $newbank->number_of_months = 48;
                    }
                    elseif($key == 9){
                        $newbank->number_of_months = 60;
                    }
                    else{
                        $month = preg_replace('/[^0-9]/', '', $datas[0]);
                        $newbank->number_of_months = $month;
                    }
                    $newbank->maturity_rate = $this->getStructuredRate($datas[3]);
                    $newbank->monthly_rate = $this->getStructuredRate($datas[1]);
                    $newbank->validity_date = $validity_date;
                    $newbank->save();
                }
            }
        }
        catch(\Exception $exception){
            print_r('Something went wrong in Ideal Finance'."\n");
        }

    }
}
