<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComparisonRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comparison_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('institute');
            $table->string('rating', 50);
            $table->integer('number_of_months')->unsigned();
            $table->decimal('maturity_rate',4,2)->nullable();
            $table->decimal('monthly_rate',4,2)->nullable();
            $table->date('validity_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comparison_rates');
    }
}
