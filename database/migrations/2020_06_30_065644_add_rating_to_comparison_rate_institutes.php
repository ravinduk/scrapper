<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRatingToComparisonRateInstitutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comparison_rate_institutes', function (Blueprint $table) {
            $table->string('rating')->nullable()->after('issuer_id');
            $table->date('rating_last_update')->nullable()->after('rating');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comparison_rate_institutes', function (Blueprint $table) {
            $table->dropColumn('rating');
            $table->dropColumn('rating_last_update');
        });
    }
}
