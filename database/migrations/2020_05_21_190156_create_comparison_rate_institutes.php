<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComparisonRateInstitutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comparison_rate_institutes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', ['Bank', 'Finance']);
            $table->string('institute_name');
            $table->string('image_link', 200)->nullable();
            $table->tinyInteger('onboarded')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->integer('issuer_id')->default(0);
            $table->text('web_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comparison_rate_institutes');
    }
}
