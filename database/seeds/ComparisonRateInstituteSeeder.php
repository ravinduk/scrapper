<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ComparisonRateInstituteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Hatton National Bank",
            'web_url' => "https://www.hnb.net/fixed-deposits-interest-rates",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Sri Lanka Savings Bank",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "DFCC Bank",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Commercial Bank of Ceylon",
            'web_url' => "https://www.combank.lk/newweb/en/personal/deposits/fixed-deposits",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Bank of Ceylon",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Sampath Bank PLC",
            'web_url' => "https://www.sampath.lk/en/personal/term-deposit-accounts/fixed-deposits",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "National Savings Bank",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Housing Development Finance Coperation Bank",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Peoples Bank",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Nations Trust Bank PLC",
            'web_url' => "https://www.nationstrust.com/deposit-rates",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "State Mortgage & Investment Bank",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "National Development Bank",
            'web_url' => "https://www.ndbbank.com/rates-and-tariffs?active=interest_rates_on_deposits",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Regional Development Bank",
            'web_url' => "https://www.rdb.lk/interest-rates/",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Associated Motor Finance Company",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Singer Finance",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Peoples Leasing & Finance PLC",
            'web_url' => "https://plc.lk/index.php/en/products/fixed-deposits-savings/fixed-deposits",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Asia Asset Finance PLC",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "UB Finance Co Ltd",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Ideal Finance Ltd",
            'web_url' => "http://idealfinance.lk/products/fixed-deposits/",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Abans Finance PLC",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Alliance Finance Co PLC",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Commercial Leasing & Finance PLC",
            'web_url' => "https://clc.lk/clc-general-fixed-deposits/",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "People Merchant Finance PLC",
            'web_url' => "http://peoplesmerchant.lk/fixed-deposit/",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "LB Finance PLC",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Bank",
            'institute_name' => "Seylan Bank",
            'web_url' => "https://www.seylan.lk/interest-rates",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Arpico Finance",
            'web_url' => "http://www.arpicofinance.com/fixed-deposits/",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Commercial Credit & Finance Plc",
            'web_url' => "https://www.clc.lk/fixed-deposit",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Multi Finance",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Prime Finance PLC",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Senkadagala Finance PLC",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Sinhaputhra Finance",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Siyapatha Finance PLC",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comparison_rate_institutes')->insert([
            'type' => "Finance",
            'institute_name' => "Vallibel Finance",
            'web_url' => "https://www.vallibelfinance.com/product/fixed-deposits",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
